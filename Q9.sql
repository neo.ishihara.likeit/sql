﻿SELECT category_name,SUM(item_price) AS total_price 
FROM 
    item i
INNER JOIN
    item_category c
ON
    i.category_id = c.category_id
GROUP BY
    category_name
;
