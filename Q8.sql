﻿select
    iii.item_id
    ,iii.item_name
    ,iii.item_price
    ,ccc.category_name
FROM item iii
INNER JOIN
     item_category ccc
ON
     iii.category_id = ccc.category_id
;
